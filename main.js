// ТЕОРІЯ
// 1. DOM - простими словами являє собою програмний інтерфейс для HTML та XML документів.
//Він структуровано подає документ і визначає те, як саме структура може бути доступна з програм,
//які можуть змінювати вміст, стиль структуру і т.д. документа.
//2. Елементи HTML — основні компоненти мови розмітки.
// innerHTML - дозволяє зчитати вміст елемента у вигляді HTML-рядка або встановити новий HTML
// innerText - дозволяє зчитати або задавати текстовий вміст елемента.
// 3.Element.attributes - повертає масиво подібний список атрибутів елементу.
// Element.classList - назви класу (ів), додавати, видаляти класи елемента.
// Element.className - клас елемента.
// Element.id - ідентифікатор елемента.
// Element.innerHTML - внутрішній HTML код елемента.
// Element.outerHTML - HTML код самого елемента і внутрішній.
// Element.innerText - внутрішній текст елемента.
// Element.textContent - текстовий вміст елемента.
// Element.localName - локальна назва елемента.
// Element.namespaceURI - простір імен URI елемента.
// Element.parentElement - батьківський елемент.
// Element.children - список дочірних елементів.
// Element.firstElementChild - перший дочірний елемент.
// Element.lastElementChild - останій дочірний елемент.
// Element.nextElementSibling - наступний елемент у батьківському списку.
// Element.previousElementSibling - попередній елемент у батьківському списку.
// Element.baseURI - базова адреса документу елемента.
// Element.ownerDocument - документ елемента.
// Element.dataset - атрибути користувача.

// 1.
let p = document.querySelectorAll("p");
p.forEach((e) => {
  e.style.background = "#ff0000";
});

// 2.
let id = document.getElementById("optionsList");
console.log(id);
console.log(id.parentElement);
console.log(id.childNodes, typeof id.childNodes);

// 3.
let testParagraph = (document.querySelector("#testParagraph").innerHTML =
  "This is a paragraph");
console.log(testParagraph);

// 4.
let conteiner = document.querySelector(".main-header").childNodes;
for (let key of conteiner) {
  key.className = "nav-items";
  console.log(key);
}

// 5.
let section = document.querySelectorAll(".section-title");
let frezz = Array.from(section);
let delet = frezz.forEach((e) => {
  e.classList.remove("section-title");
});
